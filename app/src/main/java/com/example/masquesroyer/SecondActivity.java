package com.example.masquesroyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Button btn_yes = findViewById(R.id.validate_01);
        Button btn_no = findViewById(R.id.cancel_01);

        TextView firstname = findViewById(R.id.firstname);
        TextView lastname = findViewById(R.id.lastname);
        TextView phone = findViewById(R.id.phone);

        btn_no.setOnClickListener(view -> {
            Intent i =new Intent();
            setResult(RESULT_CANCELED);
            finish();
        });

        btn_yes.setOnClickListener(view -> {
            Intent i =new Intent(this, MainActivity.class);
            i.putExtra("firstname",  firstname.getText().toString());
            i.putExtra("lastname",  lastname.getText().toString());
            i.putExtra("phone",  phone.getText().toString());
            setResult(RESULT_OK, i);
            finish();
        });
    }
}