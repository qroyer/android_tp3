package com.example.masquesroyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_OK_SECOND = 1;
    public static final int REQUEST_CODE_OK_THIRD = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_top_part = findViewById(R.id.modify_01);
        Button btn_bot_part = findViewById(R.id.modify_02);

        Intent intent =  getIntent();

        btn_top_part.setOnClickListener(view -> {
            Intent i =new Intent(this, SecondActivity.class);
            startActivityForResult(i, REQUEST_CODE_OK_SECOND);
        });

        btn_bot_part.setOnClickListener(view -> {
            Intent i =new Intent(this, ThirdActivity.class);
            startActivityForResult(i, REQUEST_CODE_OK_THIRD);
        });
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && resultCode == RESULT_OK) {
            if(requestCode == REQUEST_CODE_OK_SECOND) {
                TextView firstname = findViewById(R.id.firstname);
                TextView lastname = findViewById(R.id.lastname);
                TextView phone = findViewById(R.id.phone);

                firstname.setText(data.getStringExtra("firstname"));
                lastname.setText(data.getStringExtra("lastname"));
                phone.setText(data.getStringExtra("phone"));
            }
            if (requestCode == REQUEST_CODE_OK_THIRD) {
                TextView number = findViewById(R.id.number);
                TextView road = findViewById(R.id.road);
                TextView city = findViewById(R.id.city);
                TextView zipcode = findViewById(R.id.zipcode);

                number.setText(data.getStringExtra("number"));
                road.setText(data.getStringExtra("road"));
                city.setText(data.getStringExtra("city"));
                zipcode.setText(data.getStringExtra("zipcode"));
            }
        }
    }
}