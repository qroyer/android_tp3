package com.example.masquesroyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Button btn_yes = findViewById(R.id.validate_02);
        Button btn_no = findViewById(R.id.cancel_02);

        TextView number = findViewById(R.id.number);
        TextView road = findViewById(R.id.road);
        TextView city = findViewById(R.id.city);
        TextView zipcode = findViewById(R.id.zipcode);

        btn_no.setOnClickListener(view -> {
            Intent i =new Intent();
            setResult(RESULT_CANCELED);
            finish();
        });

        btn_yes.setOnClickListener(view -> {
            Intent i =new Intent(this, MainActivity.class);
            i.putExtra("number", number.getText().toString());
            i.putExtra("road", road.getText().toString());
            i.putExtra("city", city.getText().toString());
            i.putExtra("zipcode", zipcode.getText().toString());
            setResult(RESULT_OK, i);
            finish();
        });
    }
}